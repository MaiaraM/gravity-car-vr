# Gravity Car VR

Simulador imersivo de carrinho de rolimã

## Ferramentas Utilizadas
- Unity 2018.2.7      - Desenvolvimento do Jogo
- Linguagem C#        - Programação do Jogo
- Netbeans            - Ambiente de desenvolvimento do Monitor para Desktop
- Android Studui      - Ambiente de desenvolvimento do Plugin android
- Linguagem Java      - Programação do Plugin para comunicação Bluetooth e do Monitor
- Arduino             - Ambiente para sistemas embarcados

## Equipe de Desenvolvimento

- Bruno Cardoso Ambrosio
- Fernando Daniel Muniz
- Giulie Brito Silva
- Guilbert Adalson de Oliveira
- Jader Artur Costa
- Jonas Fernando Pires de Lima
- Leticia Yasmin Luiz
- Maiara de Carvalho Martins
- Maini Militao Brito
- Mauricio Pierangeli de Albuquerque Rother
- Paula Roberta P.S. Rodrigues
- Raul César Rodrigues da Silva Pavani
- Tiago Guerino de Oliveira Bassan
- Victoria Andressa Santos Macedo de Faria
- Kleber de Oliveira Andrade (Orientador)
